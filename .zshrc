export ZSH="/root/.oh-my-zsh"
ZSH_THEME="gentoo"
plugins=(git)

source $ZSH/oh-my-zsh.sh

alias d.c="docker-compose"
alias d="docker"
alias cdd="cd /opt/docker"
alias gi="git init"
alias gss="git status -s"
alias gaa="git add ."
alias gcm="git commit -m"
alias gp="git push"
alias update="apt update -y && apt upgrade -y && apt autoremove -y"
alias hub-update="docker-compose pull && docker-compose down && docker-compose up -d --force-recreate --build"